#ifndef FILTER_H
#define FILTER_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

void filter(char *key_word, char *buffer, int buf_size, FILE *stream);
bool is_stdout_scr(); // Checks if stdout is going to the screen or being piped

#endif