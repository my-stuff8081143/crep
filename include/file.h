#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

void file_handling(char *key_word, char *file_arg);
size_t check_file_size(char *file_path);

#endif