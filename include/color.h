#ifndef COLOR_H
#define COLOR_H

#include <string.h>

struct Color
{
   char *black;
   char *red;
   char *green;
   char *yellow;
   char *blue;
   char *magenta;
   char *cyan;
   char *white;
   char *normal;
};

void get_usr_color(char *color_argv);

#endif