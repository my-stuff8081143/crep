#ifndef ARG_H
#define ARG_H

#include <stdbool.h>
#include <string.h>

bool is_file_arg(int argc, char **argv); // Checks whether the "--file=" argument was supplied or not
bool is_color_arg(int argc, char **argv); // Checks whether the "--color=" argument was supplied or not
bool is_case_arg(int argc, char **argv); // Checks whether the "-i" argument was supplied or not

int file_arg_pos(int argc, char **argv); // Returns the position where the "--file=" is in argv. I.E "argv[2]" or "argv[3]"
int color_arg_pos(int argc, char **argv); // Returns the position where the "--file=" is in argv. I.E "argv[2]" or "argv[3]"

#endif