#include "../include/pipe.h"
#include "../include/filter.h"

void pipe_handling(char *key_word)
{
   if (!isatty(STDIN_FILENO)) // If there's stdin from pipe
   {
      char *buffer = malloc(5000);
      filter(key_word, buffer, 5000, stdin);
      free(buffer);
   }

   else
   {
      fprintf(stderr, "ERROR: NO PIPE INPUT GIVEN\n");
      exit(4);
   }
}