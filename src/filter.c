#define _GNU_SOURCE
#include "../include/filter.h"
#include "../include/color.h"

extern const struct Color color; // Global struct defined in "color.c"
extern char *g_current_color; // Global var defined in "color.c"
extern bool g_case_sense; // Global var defined in "main.c"

void filter(char *key_word, char *buffer, int buf_size, FILE *stream)
{
   char *highlight_buf;

   while (fgets(buffer, buf_size, stream) != NULL)
   {
      if (g_case_sense == false)
      {
         if (strcasestr(buffer, key_word) != NULL)
         {
            for (int i = 0; i < strlen(buffer); i++)
            {
               if (tolower(buffer[i]) == tolower(key_word[0]))
               {
                  highlight_buf = malloc(strlen(key_word) + 1);
                  highlight_buf[0] = buffer[i];

                  int sum = 1;
                  for (; tolower(buffer[i+1]) == tolower(key_word[sum]);)
                  {
                     highlight_buf[sum] = buffer[i+1];

                     i += 1;
                     sum += 1;
                  }

                  if (strlen(highlight_buf) == strlen(key_word))
                  {
                     // This if checks if stdout is going to the screen
                     // Only changes foreground color if so (otherwise "less" and things like that will look weird because of ANSI codes)
                     if (is_stdout_scr() == true)
                     {
                        printf("%s", g_current_color);
                        printf("\033[1m"); // Font bold
                        printf("%s", highlight_buf);
                        printf("%s", color.normal); // Changes foreground color to default
                        printf("\033[0m"); // Font no longer bold
                     }

                     else
                     {
                        printf("%s", highlight_buf);
                     }
                  }

                  else 
                  {
                     printf("%s", highlight_buf);
                  }
               }

               else 
               {
                  printf("%c", buffer[i]);
               }
            }
         }
      }

      else if (g_case_sense == true)
      {
         if (strstr(buffer, key_word) != NULL)
         {
            for (int i = 0; i < strlen(buffer); i++)
            {
               if (buffer[i] == key_word[0])
               {
                  highlight_buf = malloc(strlen(key_word) + 1);
                  highlight_buf[0] = buffer[i];

                  int sum = 1;
                  for (; buffer[i+1] == key_word[sum];)
                  {
                     highlight_buf[sum] = buffer[i+1];

                     i += 1;
                     sum += 1;
                  }

                  if (strlen(highlight_buf) == strlen(key_word))
                  {
                     // This if checks if stdout is going to the screen
                     // Only changes foreground color if so (otherwise "less" and things like that will look weird because of ANSI codes)
                     if (is_stdout_scr() == true)
                     {
                        printf("%s", g_current_color);
                        printf("\033[1m"); // Font bold
                        printf("%s", highlight_buf);
                        printf("%s", color.normal); // Changes foreground color to default
                        printf("\033[0m"); // Font no longer bold
                     }

                     else
                     {
                        printf("%s", highlight_buf);
                     }
                  }

                  else 
                  {
                     printf("%s", highlight_buf);
                  }
               }

               else 
               {
                  printf("%c", buffer[i]);
               }
            }
         }
      }
   }

   free(highlight_buf);
}

bool is_stdout_scr()
{
   if (isatty(STDOUT_FILENO) == 0)
   {
      return false;
   }

   else
   {
      return true;
   }
}
