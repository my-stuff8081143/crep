#include "../include/file.h"
#include "../include/pipe.h"
#include "../include/arg.h"
#include "../include/color.h"

bool g_case_sense; // "g_" prefix here means global. Later used in "filter.c"

int main(int argc, char **argv)
{
   g_case_sense = is_case_arg(argc, argv);
   bool file_arg_exists = is_file_arg(argc, argv); // "true" if user supplies "--file=" argument
   bool color_arg_exists = is_color_arg(argc, argv); // "true" if user supplies "--color=" argument

   int file_pos = file_arg_pos(argc, argv); // Finds where the "--file=" argument is in "argv". I.E "argv[2]" or "argv[3]", etc.
   int color_pos = color_arg_pos(argc, argv); // Finds where the "--color=" argument is in "argv". I.E "argv[2]" or "argv[3]", etc.

   if (argc == 1)
   {
      fprintf(stderr, "ERROR: NO ARGUMENTS WERE SUPPLIED\n");
      return 1;
   }

   if (color_arg_exists == true)
   {
      get_usr_color(argv[color_pos]);
   }

   if (file_arg_exists == false)
   {
      pipe_handling(argv[1]);
   }

   else if (file_arg_exists == true)
   {
      file_handling(argv[1], argv[file_pos]);
   }

   return 0;
}