#include "../include/color.h"

const struct Color color = // Global struct. Later used in "filter.c"
{
   "\033[30;49m", 
   "\033[31;49m", 
   "\033[32;49m", 
   "\033[33;49m", 
   "\033[34;49m",
   "\033[35;49m", 
   "\033[36;49m", 
   "\033[37;49m",
   "\033[39;49m"
};

char *g_current_color = "\033[31;49m"; // "g_" prefix here means global. Later used in "filter.c"

void get_usr_color(char *color_argv)
{
   char *arg_color = strtok(color_argv, "=");
   arg_color = strtok(NULL, "=");

   if (strcmp(arg_color, "black") == 0)
   {
      g_current_color = color.black;
   }
   
   else if (strcmp(arg_color, "red") == 0)
   {
      g_current_color = color.red;
   }

   else if (strcmp(arg_color, "green") == 0)
   {
      g_current_color = color.green;
   }

   else if (strcmp(arg_color, "yellow") == 0)
   {
      g_current_color = color.yellow;
   }

   else if (strcmp(arg_color, "blue") == 0)
   {
      g_current_color = color.blue;
   }

   else if (strcmp(arg_color, "magenta") == 0)
   {
      g_current_color = color.magenta;
   }

   else if (strcmp(arg_color, "cyan") == 0)
   {
      g_current_color = color.cyan;
   }

   else if (strcmp(arg_color, "white") == 0)
   {
      g_current_color = color.white;
   }

   else // If nothing matches
   {
      g_current_color = color.red;
   }
}