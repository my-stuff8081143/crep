#include "../include/arg.h"

bool is_file_arg(int argc, char **argv)
{
   for (int i = 1; i < argc; i++)
   {
      if ((strstr(argv[i], "--file=")) != NULL && (i != 1))
      {
         return true;
      }
   }

   return false;
}

bool is_color_arg(int argc, char **argv)
{
   for (int i = 1; i < argc; i++)
   {
      if ((strstr(argv[i], "--color=")) != NULL && (i != 1))
      {
         return true;
      }
   }

   return false;
}

bool is_case_arg(int argc, char **argv)
{
   for (int i = 1; i < argc; i++)
   {
      if ((strstr(argv[i], "-i")) != NULL && (i != 1))
      {
         return false;
      }
   }

   return true;
}

int file_arg_pos(int argc, char **argv)
{
    for (int i = 1; i < argc; i++)
   {
      if ((strstr(argv[i], "--file=")) != NULL && (i != 1))
      {
         return i;
      }
   }

   return -1;  
}

int color_arg_pos(int argc, char **argv)
{
    for (int i = 1; i < argc; i++)
   {
      if ((strstr(argv[i], "--color=")) != NULL && (i != 1))
      {
         return i;
      }
   }

   return -1;
}