#include "../include/file.h"
#include "../include/filter.h"

void file_handling(char *key_word, char *file_arg)
{
   char *file_path = strtok(file_arg, "=");
   file_path = strtok(NULL, "=");

   FILE *file = fopen(file_path, "r");
   if (file == NULL)
   {
      fprintf(stderr, "ERROR: THE GIVEN FILE DOESN'T SEEM TO EXIST\n");
      exit(3);
   }

   size_t file_size = check_file_size(file_path);
   char *buffer = malloc(file_size);
   filter(key_word, buffer, file_size, file);

   free(buffer);
   fclose(file);
}

size_t check_file_size(char *file_path)
{
   struct stat buf;
   stat(file_path, &buf);

   return buf.st_size;
}