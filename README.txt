THIS IS A GREP CLONE (WRITTEN IN C)
IT'S ABOUT 10% - 15% SLOWER THAN GREP

ARGUMENTS:
 ______________________________________________________________________________________________________________
|                                                                                                              |
| 1- "--file=<path-to-file>" CAN READ FILES (SO NO NEED FOR "cat" AND PIPING)                                  |
| I'M AWARE THIS BREAKS THE UNIX PHILOSOPHY                                                                    |
|                                                                                                              |
| 2- "--color=<color>" CAN CHANGE THE COLOR CHOSEN FOR HIGHLIGHTING (SEE THE AVAILABLE COLORS BELOW)           |
|                                                                                                              |
| 3- "-i" WILL FILTER IN A CASE-INSENSITIVE WAY                                                                |
|______________________________________________________________________________________________________________|

COLORS:
 ____________
|            |
| black      |
| red        |
| green      |
| yellow     |
| blue       |
| magenta    |
| cyan       |
| white      |
|____________|
